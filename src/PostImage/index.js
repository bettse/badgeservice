const gm = require('gm').subClass({imageMagick: true});
const multipart = require('aws-lambda-multipart-parser');

//These are 2 less than the real screen so there is a single pixel border around all sides
const WIDTH  = 262
const HEIGHT = 174

exports.handler = async (event, context) => {
  console.log(JSON.stringify(event, undefined, 2));
  if (event.headers['Content-Type'] && event.headers['Content-Type'].startsWith('multiplart/form-data')) {
    const parts = multipart.parse(event, false)
    // Log the event argument for debugging and for use in local development.
    console.log(JSON.stringify(parts, undefined, 2));
    // imageBuffer = data.Body;
    // gm(imageBuffer).resize(200, 200)

  }
  return {};
};
