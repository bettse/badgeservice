const gm = require('gm').subClass({imageMagick: true});
require('gm-base64');
const fs = require('fs')

const WIDTH = 400
const HEIGHT = 300

exports.handler = async (event, context) => {
  const today = new Date()
  const month = today.toLocaleString('en-us', { month: 'long' });
  const day = today.toLocaleString('en-us', { day: '2-digit' });

	const result = await new Promise(function(resolve, reject) {
    return gm('./background.png')
    // .gravity('North')
    .font("Helvetica.ttf")
    .fontSize(48)
    .drawText(0, 25, month, 'North')
    .fontSize(256)
    .drawText(0, 100, day, 'Center')
    .resize(WIDTH, HEIGHT) // Just to be safe

    .toBase64('png', function(err, base64) {
      if (err) {
				reject(err)
      } else {
      	resolve(base64);
			}
    });
  })
  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'image/png'
    },
    body: result,
    isBase64Encoded: true
  }
};
